function result = MultiHarris(image,s0,k,n,alpha,threshold)

figure(1) 
clf
imshow(image)
colormap('gray')
hold on
rgb=[1 0 0;0 1 0;0 0 1;1 0 1;0 1 1];

%Derivative
Dx1D=[1 0 -1];
suppressed=zeros([size(image) size(n,2)]);
R=zeros([size(image) size(n,2)]);
%Energy
for i=1:size(n,2)
sigmaIn=s0*k^n(i);
sigmaDn=0.7*sigmaIn;

kernelsz=round(3*sigmaDn)+mod(round(3*sigmaDn),2)-1;
gauss=Gausskernel([1 kernelsz],sigmaDn,1);
dgauss=convn(gauss',Dx1D,'same');

imagepaddedx=padImage(image,[1 kernelsz],0);
imagepaddedy=padImage(image,[kernelsz 1],0);

% Derivatives of Gaussian
Ix=convn(imagepaddedx,dgauss,'valid');
Iy=convn(imagepaddedy,dgauss','valid');
% % Crop
% Ix=Ix(:,ceil(size(dgauss,2)/2):end-floor(size(dgauss,2)/2));
% Iy=Iy(ceil(size(dgauss,2)/2):end-floor(size(dgauss,2)/2),:);

% Laplacian
ddgauss=ddGauss1D([-floor(round(3*sigmaIn)/2):floor(round(3*sigmaIn)/2)],sigmaIn);
Ixx=convn(image,ddgauss,'full');
Iyy=convn(image,ddgauss','full');
% Crop
Ixx=Ixx(:,ceil(size(ddgauss,2)/2):end-floor(size(ddgauss,2)/2));
Iyy=Iyy(ceil(size(ddgauss,2)/2):end-floor(size(ddgauss,2)/2),:);

F=abs(Ixx+Iyy);
suppressed(:,:,i)=NeighborhOOdSup(F,[3 3]);

GaussIn=Gausskernel([3,3],sigmaIn,0);

Ix2=Ix.^2;
Iy2=Iy.^2;
Ixy=Ix.*Iy;

Ix2=sigmaDn^2*convn(Ix2,GaussIn,'same');
Iy2=sigmaDn^2*convn(Iy2,GaussIn,'same');
Ixy=sigmaDn^2*convn(Ixy,GaussIn,'same');

R(:,:,i)=Ix2.*Iy2-Ixy.^2-alpha*(Ix2+Iy2).^2;

%threshold
choose=R(:,:,i)<threshold(1)|R(:,:,i)>threshold(2);
R(:,:,i)=R(:,:,i).*choose;
R(:,:,i)=maxSuppression(R(:,:,i),5);
[x,y]=find(R(:,:,i)~=0);
plot(y,x,'x','Color',rgb(i,:))
end
legend('n1','n2','n3','n4','n5')
idx=max3D(suppressed);
result=zeros(size(image));
for i=1:size(n,2)
    choose=R(:,:,i)~=0;
    result((idx.*choose)==i)=1;
end
[x,y]=find(result~=0);
figure(2)
clf
imagesc(image)
colormap('gray')
hold on
plot(y,x,'xr')
end