function idx=max3D(image)
idx=zeros(size(image,1),size(image,2));
for row=1:size(image,1)
    for col=1:size(image,2)
        [~,idx(row,col)]=max(image(row,col,:));
    end
end
end