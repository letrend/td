function suppressed=NeighborhOOdSup(image,kernelsz)
suppressed=zeros(size(image));
imagepadded=padImage(image,kernelsz,1);
for row=1:size(image,1)
    for col=1:size(image,2)
        suppressed(row,col)=max(max(imagepadded(row:row+kernelsz(1)-1,col:col+kernelsz(2)-1)));
    end
end
end