function suppressed=maxSuppression(image,neighbors)
dx=convn(image,repmat([1 0 -1],3,1),'same');
dy=convn(image,repmat([1 0 -1]',1,3),'same');
suppressed=zeros(size(dx));
for row=1:size(dx,1)
    for col=1:size(dy,2)
        if norm([dx(row,col) dy(row,col)])>0
            dir=[dx(row,col) dy(row,col)]/norm([dx(row,col) dy(row,col)]);
        else
            dir=[1 1];
        end
        % positive direction
        [xplus,yplus]=bresenham(row,col,row+neighbors*dir(1),col+neighbors*dir(2));
        choose=xplus>0&xplus<size(dx,1)&yplus>0&yplus<size(dx,1);
        xplus=xplus(choose);
        yplus=yplus(choose);
        if size(xplus,1)>floor(neighbors/2)
            xplus=xplus(1:floor(neighbors/2),1);
            yplus=yplus(1:floor(neighbors/2),1);
        elseif size(xplus,1)==0
            xplus=row;
            yplus=col;
        end
        % negative direction
        [xminus,yminus]=bresenham(row,col,row-neighbors*2*dir(1),col-neighbors*2*dir(2));
        choose=xminus>0&xminus<size(dx,1)&yminus>0&yminus<size(dx,1);
        xminus=xminus(choose);
        yminus=yminus(choose);
        if size(xminus,1)>floor(neighbors/2)
            xminus=xminus(1:floor(neighbors/2),1);
            yminus=yminus(1:floor(neighbors/2),1);
        elseif size(xminus,1)==0
            xminus=row;
            yminus=col;
        end
        % set value
        suppressed(row,col)=max(max(image([xplus;xminus],[yplus;yminus])));
        % suppress rest
        suppressed(xplus(2:end),yplus(2:end))=0;
        suppressed(xminus(2:end),yminus(2:end))=0;
    end
end
