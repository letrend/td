function ddGy=ddGauss1D(x,sigma)
ddGy=  (1/sigma^2*(x.^2/sigma^2-1).*exp(-0.5*(x.^2)/sigma^2));
ddGy=1/sum(ddGy(:))*ddGy;
end