%% maximum Suppression
image=imread('lena.gif');
image=image(1:2:end,1:2:end);
image=padImage(image,[3 3],0);
dx=convn(image,repmat([1 0 -1],3,1),'valid');
dy=convn(image,repmat([1 0 -1]',1,3),'valid');
image=sqrt(dx.^2+dy.^2);
imagesup=maxSuppression(image,5);
figure(1)
imshow(image,[min(image(:)) max(image(:))])
title('original')
figure(2)
imshow(imagesup,[min(imagesup(:)) max(imagesup(:))])
title('suppressed')
%% MultiHarris with harris
image=double(imread('lena.gif'));
image=image(1:3:end,1:3:end);
%image=sum(image,3);
image=image./max(image(:));
s0=1.5;
k=1.2;
alpha=0.06;
n=[0 2 5 7 9];
threshold=[-0.0005 0.0005];
MH = MultiHarris(image,s0,k,n,alpha,threshold);
%% matlab corner
figure(3)
clf
imagesc(image)
colormap('gray')
hold on
plot(corner(image),'rx')