function J=convo(image,kernel,type)
J=zeros(size(image));
M=size(kernel,1);
N=size(kernel,2);
imagepadded=zeros(size(image)+size(kernel)-1);
imagepadded(ceil(M/2):end-ceil(M/2)+1,ceil(N/2):end-ceil(N/2)+1)=image;
switch type
    case 0
       switch M>=N
           case 0
               imagepadded(ceil(M/2):end-ceil(M/2)+1,1:floor(N/2))=repmat(image(:,1),1,floor(N/2));
               imagepadded(ceil(M/2):end-ceil(M/2)+1,end-floor(N/2)+1:end)=repmat(image(:,end),1,floor(N/2));
               imagepadded(1:floor(M/2),:)=repmat(imagepadded(ceil(M/2),:),floor(M/2),1);
               imagepadded(end-floor(M/2)+1:end,:)=repmat(imagepadded(end-floor(M/2),:),floor(M/2),1);      
           case 1
               imagepadded(1:floor(M/2),ceil(N/2):end-ceil(N/2)+1)=repmat(image(1,:),floor(M/2),1);
               imagepadded(end-floor(M/2)+1:end,ceil(N/2):end-ceil(N/2)+1)=repmat(image(end,:),floor(M/2),1);
               imagepadded(:,1:floor(N/2))=repmat(imagepadded(:,ceil(N/2)),1,floor(N/2));
               imagepadded(:,end-floor(N/2)+1:end)=repmat(imagepadded(:,end-floor(N/2)),1,floor(N/2));
       end
    case 1
      switch M>=N
           case 0
               imagepadded(ceil(M/2):end-ceil(M/2)+1,1:floor(N/2))=fliplr(image(:,1:floor(N/2)));
               imagepadded(ceil(M/2):end-ceil(M/2)+1,end-floor(N/2)+1:end)=fliplr(image(:,end-floor(N/2)+1:end));
               imagepadded(1:floor(M/2),:)=flipud(imagepadded(ceil(M/2):ceil(M/2)+floor(M/2)-1,:));
               imagepadded(end-floor(M/2)+1:end,:)=flipud(imagepadded(end-(floor(M/2)+floor(M/2)):end-ceil(M/2),:));
           case 1
               imagepadded(1:floor(M/2),ceil(N/2):end-ceil(N/2)+1)=flipud(image(1:floor(M/2),:));
               imagepadded(end-floor(M/2)+1:end,ceil(N/2):end-ceil(N/2)+1)=flipud(image(end-floor(M/2)+1:end,:));
               imagepadded(:,1:floor(N/2))=fliplr(imagepadded(:,ceil(N/2):ceil(N/2)+floor(N/2)-1));
               imagepadded(:,end-floor(N/2)+1:end)=fliplr(imagepadded(:,end-(floor(N/2)+floor(N/2)):end-ceil(N/2)));
      end
end

% i=1;
% for row=1:size(imagepadded,1)-M+1
%     j=1;
%     for col=1:size(imagepadded,2)-N+1 
%         J(i,j)=sum(sum(kernel.*imagepadded(row:row+M-1,col:col+N-1)));
%         j=j+1;
%     end
%     i=i+1;
% end
for row=1:M
    for col=1:N
        J=J+kernel(row,col)*imagepadded(row:row+size(image,1)-1,col:col+size(image,2)-1);
    end
end
end