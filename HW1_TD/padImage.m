function imagepadded=padImage(image,kernelsz,type)
M=kernelsz(1);
N=kernelsz(2);
imagepadded=zeros(size(image)+kernelsz-1);
imagepadded(ceil(M/2):end-ceil(M/2)+1,ceil(N/2):end-ceil(N/2)+1)=image;
switch type
    case 0
        switch M>=N
            case 0
                imagepadded(ceil(M/2):end-ceil(M/2)+1,1:floor(N/2))=repmat(image(:,1),1,floor(N/2));
                imagepadded(ceil(M/2):end-ceil(M/2)+1,end-floor(N/2)+1:end)=repmat(image(:,end),1,floor(N/2));
                imagepadded(1:floor(M/2),:)=repmat(imagepadded(ceil(M/2),:),floor(M/2),1);
                imagepadded(end-floor(M/2)+1:end,:)=repmat(imagepadded(end-floor(M/2),:),floor(M/2),1);
            case 1
                imagepadded(1:floor(M/2),ceil(N/2):end-ceil(N/2)+1)=repmat(image(1,:),floor(M/2),1);
                imagepadded(end-floor(M/2)+1:end,ceil(N/2):end-ceil(N/2)+1)=repmat(image(end,:),floor(M/2),1);
                imagepadded(:,1:floor(N/2))=repmat(imagepadded(:,ceil(N/2)),1,floor(N/2));
                imagepadded(:,end-floor(N/2)+1:end)=repmat(imagepadded(:,end-floor(N/2)),1,floor(N/2));
        end
    case 1
        switch M>=N
            case 0
                imagepadded(ceil(M/2):end-ceil(M/2)+1,1:floor(N/2))=fliplr(image(:,1:floor(N/2)));
                imagepadded(ceil(M/2):end-ceil(M/2)+1,end-floor(N/2)+1:end)=fliplr(image(:,end-floor(N/2)+1:end));
                imagepadded(1:floor(M/2),:)=flipud(imagepadded(ceil(M/2):ceil(M/2)+floor(M/2)-1,:));
                imagepadded(end-floor(M/2)+1:end,:)=flipud(imagepadded(end-(floor(M/2)+floor(M/2)):end-ceil(M/2),:));
            case 1
                imagepadded(1:floor(M/2),ceil(N/2):end-ceil(N/2)+1)=flipud(image(1:floor(M/2),:));
                imagepadded(end-floor(M/2)+1:end,ceil(N/2):end-ceil(N/2)+1)=flipud(image(end-floor(M/2)+1:end,:));
                imagepadded(:,1:floor(N/2))=fliplr(imagepadded(:,ceil(N/2):ceil(N/2)+floor(N/2)-1));
                imagepadded(:,end-floor(N/2)+1:end)=fliplr(imagepadded(:,end-(floor(N/2)+floor(N/2)):end-ceil(N/2)));
        end
end
end