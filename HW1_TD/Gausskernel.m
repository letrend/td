function kernel=Gausskernel(sz,sigma,separate)
switch separate
    case 0
        U=repmat([-floor(sz(1)/2):floor(sz(1)/2)],sz(1),1);
        V=flipud(repmat([-floor(sz(2)/2):floor(sz(2)/2)]',1,sz(2)));
        kernel=Gauss2D(U,V,sigma);
    case 1
        if sz(1)>1
            U=[-floor(sz(1)/2):floor(sz(1)/2)];
            kernel=Gauss1D(U,sigma);
        else
            V=[-floor(sz(2)/2):floor(sz(2)/2)]';
            kernel=Gauss1D(V,sigma);
        end
end
end