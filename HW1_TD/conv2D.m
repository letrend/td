function imageconvolved=conv2D(image,kernel)
[M,N]=size(kernel);
imageconvolved=zeros(size(image)-size(kernel)+1);
for row=1:M
    for col=1:N
        imageconvolved=imageconvolved+kernel(row,col)*image(row:row+size(image,1)-M,col:col+size(image,2)-N);
    end
end

end