function Gy=Gauss1D(x,sigma)
Gy=exp(-0.5*(x.^2)/sigma^2);
Gy=1/sum(Gy(:))*Gy;
end