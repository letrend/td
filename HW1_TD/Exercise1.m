%%
img=imread('DSC02793.JPG');
imgr=double(img(1:10:end,1:10:end,3));
figure(1)
clf
subplot(2,2,1)
imshow(imgr,[min(imgr(:)) max(imgr(:))])
title('Original')
%% Exercise 1b
J=convo(imgr,ones(3),1);
subplot(2,2,2)
imshow(J,[0 max(J(:))]);
title('Mean mirror')
%% Exercise 2a 
J1=convo (imgr,Gausskernel([3,3],1,0),1);
J3=convo (imgr,Gausskernel([9,9],3,0),1);
subplot(2,2,3)
imshow(J1,[min(J1(:)) max(J1(:))]);
title('Gaussian mirror sigma=1')
subplot(2,2,4)
imshow(J3,[min(J3(:)) max(J3(:))]);
title('Gaussian mirror sigma=3')
%% Exercise 2b
Gauss11=Gausskernel([1,3],1,1);
Gauss21=Gausskernel([3,1],1,1);
Gauss13=Gausskernel([1,15],5,1);
Gauss23=Gausskernel([15,1],5,1);

diff1=(convo(convo(imgr,Gauss21,1),Gauss11,1)-J1).^2;
diff=sum(diff1(:))/(2*(302+402));
% border is always different compared to 2D conv because of different 
% clamp/mirror effects 
%% Exercise 2c
profile on
clc
imgpad=padImage(imgr,[3,3],0);
tic
temp=conv1D(imgr,Gauss11);
conv1D(temp,Gauss21);
toc;
kernel_=Gausskernel([3,3],1,1);
tic
conv2D(imgpad,kernel_);
toc;

profile viewer

%% Exercise 3a
imgdx=convo(imgr,repmat([-1 0 1],3,1),0);
imgdy=convo(imgr,repmat([-1 0 1]',1,3),0);
clf
subplot(221)
imshow(imgdx,[min(imgdx(:)) max(imgdx(:))])
title('dx')
subplot(222)
imshow(imgdy,[min(imgdy(:)) max(imgdy(:))])
title('dy')
%% Exercise 3b
subplot(223)
temp=sqrt(imgdx.^2+imgdy.^2);
imshow(temp,[min(temp(:)) max(temp(:))])
title('gradient magnitude')
subplot(224)
temp2=atan2(imgdy,imgdx);
imshow(temp2,[min(temp2(:)) max(temp2(:))])
title('angle atan(dx/dy)')
%% Exercise 3c
figure(3)
clf
subplot(221)
imshow(imgdx,[min(imgdx(:)) max(imgdx(:))])
title('dx')
subplot(222)
imshow(imgdy,[min(imgdy(:)) max(imgdy(:))])
title('dy')
subplot(223)
temp=convo(imgr,convo(Gauss23,repmat([-1 0 1],3,1),0),0);
imshow(temp,[min(temp(:)) max(temp(:))])
title('dx with gauss')
subplot(224)
temp=convo(imgr,convo(Gauss13,repmat([-1 0 1]',1,3),0),0);
imshow(temp,[min(temp(:)) max(temp(:))])
title('dy with gauss')








