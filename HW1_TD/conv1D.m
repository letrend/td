function imageconvolved=conv1D(image,kernel)
[M,N]=size(kernel);
imageconvolved=zeros(size(image)-size(kernel)+1);
switch M>N
    case 1
        for row=1:M
            imageconvolved=imageconvolved+kernel(row,1)*image(row:row+size(image,1)-M,1:1+size(image,2)-N);
        end
    case 0
        for col=1:N
            imageconvolved=imageconvolved+kernel(1,col)*image(1:1+size(image,1)-M,col:col+size(image,2)-N);
        end
end
end