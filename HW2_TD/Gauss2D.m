function Gz=Gauss2D(x,y,sigma)
Gz=exp(-0.5*(x.^2+y.^2)/sigma^2);
Gz=1/sum(Gz(:))*Gz;
end