function [ imagefiltered ] = medianFilter( image, kernelsize )
% median filtering
imagefiltered=zeros(size(image));
image=padImage(image,kernelsize,0);
i=1;
for row=1:size(image,1)-kernelsize(1)
    j=1;
    for col=1:size(image,2)-kernelsize(2)
        temp=image(row:row+kernelsize(1)-1,col:col+kernelsize(2)-1);
        imagefiltered(i,j)=median(temp(:));
        j=j+1;
    end
    i=i+1;
end

end

