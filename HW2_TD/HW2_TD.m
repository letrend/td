%% Excercise 1c:
image=double(imread('lena.gif'));
kernelsize=[3 3];
gauss=Gausskernel(kernelsize,1,0);
imagesalty=saltnpepper(image,0.01);
imagenoisy=gaussianNoise(image,20);
figure(1)
clf
subplot(231)
imshow(imagesalty,[min(image(:)) max(image(:))])
title('image salt n pepper')
subplot(234)
imshow(imagenoisy,[min(image(:)) max(image(:))])
title('image noise')
subplot(232)
imshow(convn(imagesalty,gauss,'same'),[min(image(:)) max(image(:))])
title('gauss')
subplot(235)
imshow(convn(imagenoisy,gauss,'same'),[min(image(:)) max(image(:))])
title('gauss')
subplot(233)
imshow(medianFilter(imagesalty,kernelsize),[min(image(:)) max(image(:))])
title('median')
subplot(236)
imshow(medianFilter(imagenoisy,kernelsize),[min(image(:)) max(image(:))])
title('median')
%% Exercise 2a
% Both are taking neighboring pixels into account. The domain filter
% accounts for the distance, causing closer neighbors to be more important.
% The range filter accounts for the similarity of neighboring pixels.
% Approaching zero as the neighboring pixels become less similar in
% intensity and being one as they have the same Intensity.
%% Exercise 2d
figure(2)
clf
subplot(231)
imshow(bilateralFilter(image,[3 3],1,1),[min(image(:)) max(image(:))])
title('bilateral sigma=3')
subplot(232)
imshow(bilateralFilter(image,[15 15],5,5),[min(image(:)) max(image(:))])
title('bilateral sigma=5')
subplot(233)
imshow(bilateralFilter(image,[31 31],10,10),[min(image(:)) max(image(:))])
title('bilateral sigma=10')
subplot(234)
imshow(convn(image,Gausskernel([3 3],1,0),'same'),[min(image(:)) max(image(:))])
title('gauss sigma=3')
subplot(235)
imshow(convn(image,Gausskernel([15 15],5,0),'same'),[min(image(:)) max(image(:))])
title('gauss sigma=5')
subplot(236)
imshow(convn(image,Gausskernel([31 31],10,0),'same'),[min(image(:)) max(image(:))])
title('gauss sigma=10')
% smoothing but sharp edges
%% Exercise 2e
% A complete convolutional approach is not possible, because of the
% Euclidean distance for the range filter, which cannot be expressed as
% a constant convolutional kernel. (domain filter alone would be fine)
