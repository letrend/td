function [ imagefiltered ] = bilateralFilter( image,kernelsize, sigmad, sigmar )
imagepadded=padImage(image,kernelsize,0);
imagefiltered=zeros(size(imagepadded)-kernelsize+1);
domain=zeros(kernelsize);
i=1;
for row=-floor(kernelsize(1)/2):1:floor(kernelsize(1)/2)
    j=1;
    for col=-floor(kernelsize(2)/2):1:floor(kernelsize(2)/2)
        domain(i,j)=exp(-0.5*(sqrt(row^2+col^2)/sigmad)^2);
        j=j+1;
    end
    i=i+1;
end
i=1;
for row=1:size(imagepadded,1)-kernelsize(1)
    j=1;
    for col=1:size(imagepadded,2)-kernelsize(2)
        temp=imagepadded(row:row+kernelsize(1)-1,col:col+kernelsize(2)-1)-imagepadded(row+floor(kernelsize(1)/2),col+floor(kernelsize(2)/2));
        range=exp(-0.5*(sqrt(temp.^2)/sigmar).^2);
        imagefiltered(i,j)=sum(sum(range.*domain.*imagepadded(row:row+kernelsize(1)-1,col:col+kernelsize(2)-1)))...
            *1/sum(sum(range.*domain));
        j=j+1;
    end
    i=i+1;
end

end

