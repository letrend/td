function image=saltnpepper(image,noiselevel)
saltx=randi([1 size(image,1)],round(numel(image)*noiselevel),1);
salty=randi([1 size(image,2)],round(numel(image)*noiselevel),1);
for i=1:size(saltx,1)
    image(saltx(i,1),salty(i,1))=max(image(:));
end

pepperx=randi([1 size(image,1)],round(numel(image)*noiselevel),1);
peppery=randi([1 size(image,2)],round(numel(image)*noiselevel),1);
for i=1:size(pepperx,1)
    image(pepperx(i),peppery(i))=min(image(:));
end
end