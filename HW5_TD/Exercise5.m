clear
close all
%%
fern=Fern(20,9,[30 30]);
%%
img=imread('images/img1.ppm');
figure(1)
clf
imshow(img)
%% ROI
k = waitforbuttonpress;
point1 = get(gca,'CurrentPoint');  %button down detected
rectregion = rbbox;  %%%return figure units
point2 = get(gca,'CurrentPoint');%%%%button up detected
point1 = point1(1,1:2); %%% extract col/row min and maxs
point2 = point2(1,1:2);
lowerleft = min(point1, point2);
upperright = max(point1, point2); 
ymin = round(lowerleft(1)); %%% arrondissement aux nombrs les plus proches
ymax = round(upperright(1));
xmin = round(lowerleft(2));
xmax = round(upperright(2));
img_ROI=rgb2gray(img(xmin:xmax,ymin:ymax,:));
img_ROI2=img;
img_ROI2(xmin:xmax,ymin:ymax,1)=255;
clf
subplot(121)
imshow(img_ROI2)
subplot(122)
imagesc(img_ROI)
colormap('gray')
axis equal
%% Robust Harris Keypoints
robust_keypoints=robustFeaturePoints(img_ROI);
subplot(122)
hold on 
plot(robust_keypoints(:,1),robust_keypoints(:,2),'rx')
%% Train ferns
tic;
fern=train(fern,single(rgb2gray(img)),[xmin ymin;xmax ymax],robust_keypoints);
disp('trainingtime in minutes')
disp(toc/60)
save('fern_trained.mat','fern')
%% Classify
load('fern_trained_20_9_30x30_10000.mat','fern')
img=imread('images/img2.ppm');
[thisisthat,probs]=detect(fern,rgb2gray(img));
figure(1)
hold on
plot(round(thisisthat(:,1)),round(thisisthat(:,2)),'go')
for j=1:size(thisisthat,1)
text(double(thisisthat(:,1)),double(thisisthat(:,2)),num2str(thisisthat(:,3)))
end
%% RANSAC
% H=RANSAC([fliplr(robust_keypoints) ones(size(robust_keypoints,1),1)]',[fliplr(thisisthat(:,1:2)) ones(size(thisisthat,1),1)]',0.8,3,size(robust_keypoints,1));
% %% 
% figure(2)
% clf
% ROI_warped=H*[ymin ymax;xmin xmax;1 1];
% ROI_warped=[ROI_warped(:,1)./ROI_warped(3,1) ROI_warped(:,2)./ROI_warped(3,2)];
% img(ROI_warped(1,2):ROI_warped(2,2),ROI_warped(1,1):ROI_warped(2,1),2)=255;
% imshow(img)
% 
