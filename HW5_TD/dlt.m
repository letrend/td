function H=dlt(x,xdash,normalize,U,T)
n=size(x,2);
%% Generating A
A=zeros(2*n,3);
A(2:2:end,:)=(repmat(xdash(3,:),3,1).*x)';
B=zeros(2*n,3);
B(1:2:end,:)=(repmat(-xdash(3,:),3,1).*x)';
C=zeros(2*n,3);
C(1:2:end,:)=(repmat(xdash(2,:),3,1).*x)';
C(2:2:end,:)=(repmat(-xdash(1,:),3,1).*x)';
D=[A B C];
%% SVD
[~,~,V] = svd(D);
H=reshape(V(:,end),3,3)';
if normalize
    H=T\H*U;
end
H=H/H(3,3);
end