classdef Fern
    
    properties
        numberOfFerns
        depthOfFerns
        patchSize
        posteriors
        numberKeypoints
        robust_keypoints
        posx1
        posy1
        posx2
        posy2
    end
    
    methods
        function obj=Fern(nrFerns,depth,patchSize)
            obj.numberOfFerns=nrFerns;
            obj.depthOfFerns=depth;
            obj.patchSize=patchSize;
            obj.posx1=randi([1 patchSize(1)],depth,nrFerns);
            obj.posy1=randi([1 patchSize(2)],depth,nrFerns);
            obj.posx2=randi([1 patchSize(1)],depth,nrFerns);
            obj.posy2=randi([1 patchSize(2)],depth,nrFerns);
        end
        
        function fern=train(fern,img,ROI,keypoints)
            %keypoints=round(keypoints);
            maxNrPatches=10000;
            maxBorder=round((ROI(2,:)-ROI(1,:))+fern.patchSize*2*1.5);
            keypoints_aug=keypoints+repmat(fliplr(maxBorder./2-(ROI(2,:)-ROI(1,:))./2),size(keypoints,1),1);
            centerROI=ROI(1,:)+(ROI(2,:)-ROI(1,:))./2;
            aug_ROI=img(round(centerROI(1)-maxBorder(1)/2):round(centerROI(1)+maxBorder(1)/2),...
                round(centerROI(2)-maxBorder(2)/2):round(centerROI(2)+maxBorder(2)/2));
            fern.posteriors=ones([2^fern.depthOfFerns fern.numberOfFerns size(keypoints,1)])*0.0000001;
            for j=1:size(keypoints,1) % for every keypoint
                phi   = [0 (rand(1,maxNrPatches-1)*2-1)*pi];   % [-pi;pi]
                theta = [0 (rand(1,maxNrPatches-1)*2-1)*pi];   % [-pi;pi]
                s     = [1 (rand(1,maxNrPatches-1)*0.9)+0.6;
                    1 (rand(1,maxNrPatches-1)*0.9)+0.6];
                for iter=1:maxNrPatches  % for maxNrPatches warping
                    Rphi  = [cos(phi(iter)) -sin(phi(iter)) 0;...
                        sin(phi(iter))  cos(phi(iter)) 0;
                        0              0        1];
                    Rtheta= [cos(theta(iter)) -sin(theta(iter)) 0;...
                        sin(theta(iter))  cos(theta(iter)) 0;
                        0                   0       1];
                    Scale=[s(1,iter)        0         0;...
                        0         s(2,iter)      0;...
                        0           0         1];   % [0.6;1.5]
                    aug_patch=aug_ROI(round(keypoints_aug(j,2)-fern.patchSize(1)*1.5):round(keypoints_aug(j,2)+fern.patchSize(1)*1.5),...
                        round(keypoints_aug(j,1)-fern.patchSize(2)*1.5):round(keypoints_aug(j,1)+fern.patchSize(2)*1.5));
                    aug_ROI_warped=affineTransform(aug_patch,Rtheta*Scale*Rphi,round(size(aug_patch)/2));
                    patch=aug_ROI_warped(round(size(aug_ROI_warped,1)/2-fern.patchSize(1)/2):round(size(aug_ROI_warped,1)/2+fern.patchSize(1)/2)-1,...
                        round(size(aug_ROI_warped,2)/2-fern.patchSize(2)/2):round(size(aug_ROI_warped,2)/2+fern.patchSize(2)/2)-1);
                    figure(2)
                    clf
                    imagesc(img(ROI(1,1):ROI(2,1),ROI(1,2):ROI(2,2)))
                    hold on
                    plot(keypoints(j,1),keypoints(j,2),'rx')
                    axis equal
                    figure(3)
                    clf
                    imagesc(aug_ROI_warped)
                    hold on
                    plot(keypoints_aug(j,1),keypoints_aug(j,2),'rx')
                    imagesc(patch)
                    axis equal
                    
                    res=patch(sub2ind(fern.patchSize,fern.posx1,fern.posy1))<=...
                        patch(sub2ind(fern.patchSize,fern.posx2,fern.posy2));
                    fern.posteriors((0:fern.numberOfFerns-1)*2^fern.depthOfFerns+(bi2de(res')+1)'+(j-1)*fern.numberOfFerns*2^fern.depthOfFerns)=...
                        fern.posteriors((0:fern.numberOfFerns-1)*2^fern.depthOfFerns+(bi2de(res')+1)'+(j-1)*fern.numberOfFerns*2^fern.depthOfFerns)+1;
                    if mod(iter,100)==0
                        clc
                        disp('keypoint  warp')
                        disp([j iter])
                    end
                end
            end
            % Normalizing ferns
            fern.posteriors=fern.posteriors./repmat(sum(fern.posteriors),2^fern.depthOfFerns,1,1);
            fern.numberKeypoints=size(fern.posteriors,3);
            fern.robust_keypoints=keypoints;
        end
        
        function [thisisthat,mostpropable]=detect(fern,img)
%             fern.posteriors=fern.posteriors;
            aug_img=zeros(size(img)+fern.patchSize*4);
            aug_img(fern.patchSize(1)*2:fern.patchSize(1)*2+size(img,1)-1,...
                fern.patchSize(2)*2:fern.patchSize(2)*2+size(img,2)-1)=img;
            aug_img=convn(aug_img,fspecial('gaussian',[5 5],1),'same');
            keypoints=detectHarrisFeatures(aug_img,'MinQuality', 0.001);
            clf
            imagesc(aug_img)
            hold on
            plot(keypoints.Location(:,1),keypoints.Location(:,2),'rx')
            vote=zeros(size(keypoints,1),2);
            for i=1:size(keypoints,1)
                patch=aug_img(round(keypoints.Location(i,2)-fern.patchSize(1)/2):round(keypoints.Location(i,2)+fern.patchSize(1)/2)-1,...
                              round(keypoints.Location(i,1)-fern.patchSize(2)/2):round(keypoints.Location(i,1)+fern.patchSize(2)/2)-1);
                res=patch(sub2ind(fern.patchSize,fern.posx1,fern.posy1))<=...
                    patch(sub2ind(fern.patchSize,fern.posx2,fern.posy2));
                choose=repmat((0:fern.numberOfFerns-1)*2^fern.depthOfFerns+(bi2de(res')+1)',fern.numberKeypoints,1)...
                      +repmat((0:fern.numberKeypoints-1)*2^fern.depthOfFerns*fern.numberOfFerns,fern.numberOfFerns,1)';
                classprobabiltiy=sum(log2(fern.posteriors(choose)),2);
                [vote(i,1),vote(i,2)]=max(classprobabiltiy);
            end
            [propability,I]=sort(vote(:,1),'descend');
            mostpropable=propability(1:size(fern.posteriors,3));
            thisisthat=[keypoints.Location(I(1:size(fern.posteriors,3)),:) vote(I(1:size(fern.posteriors,3)),2)];
            %-repmat(fern.patchSize*2,30,1) ...
            
        end
    end
    
end

