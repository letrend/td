function robust_keypoints=robustFeaturePoints(img)
% augment image with zeros border
maxBorder=round([size(img,1) size(img,2)]*2*1.5);
aug_img=zeros(maxBorder);%rand(maxBorder)*255;
aug_img(round(maxBorder(1)/2-size(img,1)/2+1):round(maxBorder(1)/2+size(img,1)/2),...
    round(maxBorder(2)/2-size(img,2)/2+1):round(maxBorder(2)/2+size(img,2)/2))=img;
% harris keypoints
harris0=detectHarrisFeatures(aug_img,'MinQuality', 0.001);
voting=zeros(size(aug_img));
for i=1:100
    phi   = (rand*2-1)*pi;   % [-pi;pi]
    theta = (rand*2-1)*pi;   % [-pi;pi]
    Rphi  = [cos(phi) -sin(phi) 0;...
        sin(phi)  cos(phi) 0;
        0         0     1];
    Rtheta= [cos(theta) -sin(theta) 0;...
        sin(theta)  cos(theta) 0;
        0            0     1];
    Scale=[(rand*0.9)+0.6        0         0;...
        0          (rand*0.9)+0.6 0;...
        0               0         1];   % [0.6;1.5]
    img_warped=affineTransform(aug_img,Rtheta*Scale*Rphi,round(size(aug_img)./2));%
    %     img_warped=affineTransform(img_warped,Scale);
    %     img_warped=affineTransform(img_warped,Rtheta);
    
    harris1=detectHarrisFeatures(img_warped);
    keypoints_t=[harris1.Location(:,1)'-size(aug_img,2)/2;...
        harris1.Location(:,2)'-size(aug_img,1)/2];
    keypoints_t=Rphi(1:2,1:2)\keypoints_t;
    keypoints_t=Scale(1:2,1:2)\keypoints_t;
    keypoints_t=Rtheta(1:2,1:2)\keypoints_t;
    keypoints_t=round([keypoints_t(1,:)+size(aug_img,2)/2;...
        keypoints_t(2,:)+size(aug_img,1)/2]);
    
    figure(4)
    clf
    imagesc(aug_img)
    hold on
    plot(harris0.Location(:,1),harris0.Location(:,2),'rx')
    plot(keypoints_t(1,:),keypoints_t(2,:),'go')
    
    choose=keypoints_t>1&keypoints_t<=repmat([size(aug_img,1);size(aug_img,2)],1,size(keypoints_t,2));
    voting(keypoints_t(2,choose(1,:)),keypoints_t(1,choose(2,:)))=voting(keypoints_t(1,choose(1,:)),keypoints_t(2,choose(2,:)))+1;
    %     matchidx=matchFeatures(keypoints_t',harris0.Location,'Unique',true);
    %     robust_keypoints=harris0.Location(matchidx(:,2),:);
end
neighbors=3;
occurences=zeros(size(harris0.Location,1),1);
for j=1:size(harris0.Location,1) % for every harris keypoint in img_aug
    xpos=round(harris0.Location(j,2));
    ypos=round(harris0.Location(j,1));
    occurences(j)=sum(sum(voting(xpos-neighbors:xpos+neighbors,ypos-neighbors:ypos+neighbors)));
end
occurences=occurences./max(occurences);
threshold=0.7;
robust_keypoints=harris0.Location(occurences>threshold,:);
robust_keypoints=robust_keypoints-repmat([round(size(aug_img,2)/2)-size(img,2)/2 ...
    round(size(aug_img,1)/2)-size(img,1)/2],size(robust_keypoints,1),1);
figure(2)
clf
imagesc(voting)
hold on
plot(harris0.Location(:,1),harris0.Location(:,2),'rx')
axis equal
figure(3)
clf
imagesc(img)
hold on
plot(robust_keypoints(:,1),robust_keypoints(:,2),'go')
axis equal
end