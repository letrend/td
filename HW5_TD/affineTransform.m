function img_out=affineTransform(img,R,center)
img = padarray(img, [0 0 1],'both');
x=-floor(size(img,1)/2):floor(size(img,1)/2)-1;
y=-floor(size(img,2)/2):floor(size(img,2)/2)-1;
if size(x)~=size(img,1)
    x=-floor(size(img,1)/2):floor(size(img,1)/2);
end
if size(y)~=size(img,2)
    y=-floor(size(img,2)/2):floor(size(img,2)/2);
end
x=x-center(1)+round(size(img,1)/2);
y=y-center(2)+round(size(img,2)/2);
[x,y,z] = ndgrid(x,y,-1:1); 
% x=single(x);
% y=single(y);
% z=single(z);
tmp = R*[x(:)'; y(:)'; z(:)']; % 2 by numel(img)
xi = reshape(tmp(1,:),size(x)); % new x-indicies
yi = reshape(tmp(2,:),size(y)); % new y-indicies
zi = reshape(tmp(3,:),size(z));
imgrot = interpn(x,y,z,img,xi,yi,zi,'linear',0); % interpolate from old->new indicies
img_out=imgrot(:,:,2);
end