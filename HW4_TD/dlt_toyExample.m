x= [1:10;1:10];
x = [x;ones(1,size(x,2))];
alpha=pi/7;
Htrue=[cos(alpha) sin(alpha) 0;-sin(alpha) cos(alpha) 0;0 0 1];
y = Htrue * x;
y(1:2,:)=y(1:2,:)+1;
y(1:2,:)=2*y(1:2,:);

[X0,U] = normalizeFcn(x);
[X1,T] = normalizeFcn(y);
H=dlt(X0,X1,1,U,T);
x_s=H*x;

figure(1)
clf
hold on
plot(x(1,:),x(2,:),'xr')
plot(y(1,:),y(2,:),'xb')
plot(x_s(1,:),x_s(2,:),'og')
axis equal