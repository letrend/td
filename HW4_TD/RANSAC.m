function H=RANSAC(X,Y,p,td,tset)
N=inf;
sample_count=1;
done=0;
best=0;
bestinliers=0;
while N>sample_count && ~done
    rndset=randi([1 size(X,2)],1,4);
    [X0,U]=normalizeFcn(X(:,rndset));
    [X1,T]=normalizeFcn(Y(:,rndset));
    H=dlt(X0,X1,1,U,T);
    X1_dash=H*X;
    X1_dash=X1_dash./repmat(X1_dash(3,:),3,1);
    inliers=sqrt(sum((X1_dash(1:2,:)-Y(1:2,:)).^2))<td;
    if sum(inliers)>best
    	bestinliers=inliers;
    end
    if sum(inliers)>=tset
        done=1;
    end
    epsilon=1-sum(inliers)/numel(inliers);
    N=round(log(1-p)/log(1-(1-epsilon)^4));
    sample_count=sample_count+1;
end
[X0,U]=normalizeFcn(X(:,bestinliers));
[X1,T]=normalizeFcn(Y(:,bestinliers));
H=dlt(X0,X1,1,U,T);
end