function img_out=stitch(img0,img1,H)
img_indices=zeros([size(img0) 3]);
for row=1:size(img0,1)
    for col=1:size(img0,2)
        img_indices(row,col,:)=H\[col;row;1];
        img_indices(row,col,:)=round(img_indices(row,col,:)/img_indices(row,col,3));
    end
end
minIndices=[min(min(img_indices(:,:,1))) min(min(img_indices(:,:,2)))];
maxIndices=[max(max(img_indices(:,:,1))) max(max(img_indices(:,:,2)))];
choose=img_indices(:,:,1)>=1&img_indices(:,:,2)>=1;%&img_indices(:,:,1)<=size(img0,2)&img_indices(:,:,2)<=size(img0,1);
x=img_indices(:,:,1);
y=img_indices(:,:,2);
x=x(choose);
y=y(choose);
px=img1(choose);
img_out=zeros([max(y) max(x)]);
img_out(1:size(img0,1),1:size(img0,2))=img0;
for i=1:numel(x)
img_out(y(i),x(i))=px(i);
end
end