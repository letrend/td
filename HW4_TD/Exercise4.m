clear
close all
run('vlfeat-0.9.19/toolbox/vl_setup')
image0=imread('tum_mi_1.JPG');
image1=imread('tum_mi_2.JPG');
image0=single(rgb2gray(image0));
image1=single(rgb2gray(image1));
%% SIFT image0
[f0,d0]=vl_sift(image0);
figure(1)
clf
imagesc(image0);
colormap('gray')
hold on ;
perm = randperm(size(f0,2)) ;
sel  = perm(1:size(f0,2)) ;
h1   = vl_plotframe(f0(:,sel)) ; set(h1,'color','k','linewidth',3) ;
h2   = vl_plotframe(f0(:,sel)) ; set(h2,'color','y','linewidth',2) ;
%% SIFT image1
[f1,d1]=vl_sift(image1);
figure(2)
clf
imagesc(image1);
colormap('gray')
hold on ;
perm = randperm(size(f1,2)) ;
sel  = perm(1:size(f1,2)) ;
h1   = vl_plotframe(f1(:,sel)) ; set(h1,'color','k','linewidth',3) ;
h2   = vl_plotframe(f1(:,sel)) ; set(h2,'color','y','linewidth',2) ;
%% Normalizing Data, 1
X0=[f0(1:2,:);ones(1,size(f0,2))];
X1=[f1(1:2,:);ones(1,size(f1,2))];
%% Matching Descriptors
[matches, scores] = vl_ubcmatch(d0,d1) ;
figure(1)
hold on
plot(X0(1,matches(1,:)),X0(2,matches(1,:)),'rx')
figure(2)
hold on
plot(X1(1,matches(2,:)),X1(2,matches(2,:)),'rx')
%% DLT
H=RANSAC(X0(:,matches(1,:)),X1(:,matches(2,:)),0.8,3,50);
img=stitch(image0,image1,H);
figure(3)
clf
imagesc(img)
colormap('gray')


